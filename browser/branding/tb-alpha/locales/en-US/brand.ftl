# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

# The shortened application name for Tor Browser. Shared between versions.
-brand-shorter-name = Tor Browser
# The default application name for the "alpha" release.
-brand-short-name = Tor Browser Alpha
# The full application name for the "alpha" release.
-brand-full-name = Tor Browser Alpha
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor Browser
# The name of the Tor Project organisation.
-vendor-short-name = Tor Project
# "Tor" is a trademark names, so should not be translated (not including the quote marks, which can be localized).
# "The Tor Project, Inc." is an organisation name.
trademarkInfo = “Tor” and the Tor logo are registered trademarks of The Tor Project, Inc.
